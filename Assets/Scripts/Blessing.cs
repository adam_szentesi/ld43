﻿using UnityEngine;

namespace Ludum
{
    public class Blessing : MonoBehaviour
    {
        protected Game _Game;
        public AudioSource BlessSound;
        public float ReloadTime = 1.0f;
        protected float _LastBless = 0;

        public int Index = 0;

        public virtual void Init(Game game, int index)
        {
            _Game = game;
            Index = index;
            PostInit();
            BlessSound.Play();
        }
        public virtual void PostInit() { }

        private void Update()
        {
            if (_LastBless > 0)
            {
                _LastBless -= Time.deltaTime;
            }

            PostUpdate();

            if (_LastBless <= 0)
            {
                Bless();
            }
        }
        protected virtual void PostUpdate() { }

        protected virtual void Bless() { }
    }
}