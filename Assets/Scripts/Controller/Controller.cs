﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class Controller
{
    private static readonly List<KeyCode>[] _KeyCodes = new List<KeyCode>[Enum.GetValues(typeof(ControllerAction)).Length];

    static Controller()
    {
        for (int i = 0; i < _KeyCodes.Length; i++)
        {
            _KeyCodes[i] = new List<KeyCode>();
        }

        MapActionToKey(ControllerAction.MoveForward, KeyCode.W);
        MapActionToKey(ControllerAction.MoveBackward, KeyCode.S);
        MapActionToKey(ControllerAction.MoveLeftward, KeyCode.A);
        MapActionToKey(ControllerAction.MoveRightward, KeyCode.D);
        MapActionToKey(ControllerAction.Quit, KeyCode.Escape);
    }

    public static void MapActionToKey(ControllerAction action, KeyCode key)
    {
        List<KeyCode> keyCodes = _KeyCodes[(int)action];
        if (!keyCodes.Contains(key))
        {
            keyCodes.Add(key);
        }
    }

    public static bool GetAction(ControllerAction action)
    {
        List<KeyCode> keyCodes = _KeyCodes[(int)action];
        if (keyCodes != null)
        {
            foreach (KeyCode keyCode in keyCodes)
            {
                return Input.GetKey(keyCode);
            }
        }

        return false;
    }

    public static bool GetActionDown(ControllerAction action)
    {
        List<KeyCode> keyCodes = _KeyCodes[(int)action];
        if (keyCodes != null)
        {
            foreach (KeyCode keyCode in keyCodes)
            {
                return Input.GetKeyDown(keyCode);
            }
        }

        return false;
    }

    public static bool GetActionUp(ControllerAction action)
    {
        List<KeyCode> keyCodes = _KeyCodes[(int)action];
        if (keyCodes != null)
        {
            foreach (KeyCode keyCode in keyCodes)
            {
                return Input.GetKeyUp(keyCode);
            }
        }

        return false;
    }

}

