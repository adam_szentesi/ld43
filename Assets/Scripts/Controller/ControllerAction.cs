﻿public enum ControllerAction
{
    MoveRightward,
    MoveUpward,
    MoveForward,
    MoveLeftward,
    MoveDownward,
    MoveBackward,
    Quit,
}