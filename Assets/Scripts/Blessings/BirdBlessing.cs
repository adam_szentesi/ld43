﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    public class BirdBlessing : Blessing
    {
        public float Altitude = 1;
        public float Frequency = 1;
        public float Radius = 1;
        public Transform rendererTransform;
        public float angle;

        public override void PostInit()
        {
            angle = Random.Range(0.0f, 360.0f);
            rendererTransform.localPosition = new Vector3(Radius, Altitude, 0);
        }

        protected override void PostUpdate()
        {
            angle -= Time.deltaTime * 360 * Frequency;

            if (angle < 0)
            {
                angle += 360;
            }

            transform.position = _Game.Herd.HerdShepherd.transform.position;
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }

        protected override void Bless()
        {
            _Game.Herd.Heal(5);

            _LastBless = ReloadTime;
        }

    }
}