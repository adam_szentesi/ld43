﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    public class HammerBlessing : Blessing
    {
        public float Altitude = 20;
        public float Speed = 3;
        public int Damage = 200;
        public int Radius = 100;

        private float _Timer = 0;
        private Vector3 _OriginalPosition;
        private Color _OriginalColor;

        public override void PostInit()
        {
            _OriginalPosition = _Game.Herd.HerdShepherd.transform.position;
            _OriginalColor = GetComponent<MeshRenderer>().material.color;
            transform.position = _OriginalPosition + new Vector3(0, Altitude, 0);
            transform.rotation = Quaternion.Euler(0, 0, 0);
            _Timer = 1;
        }

        protected override void PostUpdate()
        {
            if (_Timer > 0)
            {
                transform.position = _OriginalPosition + new Vector3(0, Altitude * _Timer, 0);
            }
            else
            {
                GetComponent<MeshRenderer>().material.color = new Color(_OriginalColor.r, _OriginalColor.g, _OriginalColor.b, 1 + _Timer);
                transform.position = _OriginalPosition;
            }

            _Timer -= (Speed / Altitude) * Time.deltaTime;

            if(_Timer < -1)
            {
                _Game.RemoveBlessing(Index);

                foreach (Enemy enemy in _Game.Enemies)
                {
                    float distanceSquared = (enemy.transform.position - _OriginalPosition).sqrMagnitude;
                    float radiusSquared = Radius * Radius;
                    if (distanceSquared < radiusSquared)
                    {
                        enemy.TakeDamage(Damage);
                    }
                }
            }
        }

    }
}
