﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    public enum Deity
    {
        Rod,
        Morana,
        Perun,
        Svantovit,
        Svarog,
    }

    public class Game : MonoBehaviour
    {
        public LevelTerrain LevelTerrain;
        public Cursor GameCursor;
        public Herd Herd;
        public WindowManager WindowManager;
        public Window GameOverWindow;
        public List<Enemy> Enemies;

        public List<Blessing> Blessings;
        public Transform BlessingsParent;

        private void Start()
        {
            Herd.Init(this);
            InitEnemies();
        }

        void Update ()
        {
            HandleInput();
            HandleEnemies();
        }

        private void HandleInput()
        {
            if (Input.GetMouseButtonUp(0))
            {
                bool success = false;
                
                Vector3 cursorPosition = LevelTerrain.GetPositionFromMouse(Input.mousePosition, ref success);
                if (success)
                {
                    GameCursor.Move(cursorPosition);
                }

                Altar targetAltar = LevelTerrain.FindAltar(Input.mousePosition, LevelTerrain, ref success);
                if (success)
                {
                    Herd.TargetAltar = targetAltar;
                }
                else
                {
                    Herd.TargetAltar = null;
                }
            }

            if (Controller.GetActionUp(ControllerAction.Quit))
            {
                Application.Quit();
            }
        }

        private void HandleEnemies()
        {
            for(int i = Enemies.Count - 1; i >= 0; i--)
            {
                if (!Enemies[i].IsAlive)
                {
                    Enemies.RemoveAt(i);
                }
                else
                {
                    Enemies[i].HuntTarget(Herd, LevelTerrain);
                }
            }
        }

        private void InitEnemies()
        {
            foreach (Enemy enemy in Enemies)
            {
                enemy.Init(LevelTerrain);
            }
        }

        public void ActivateAltar(Altar altar)
        {
            string message = "";
            if (!altar.Active)
            {
                altar.Active = true;

                //debug
                message = Herd.Sacrifice(altar.Deity.AcceptedOffering);
            }

            //game finished hack
            if (altar.Deity.Name == Deity.Rod)
            {
                GameFinished();
            }
            else
            {
                if (!WindowManager.IsOpen())
                {
                    WindowManager.OpenWindow(message, 3);
                }
            }
        }

        public void DeactivateAltar(Altar altar)
        {
            altar.Active = false;
            //WindowManager.CloseWindow();
        }

        public void SummonBlessing(GameObject blessingPrefab, Vector3 position, Quaternion rotation)
        {
            GameObject blessingGO = Instantiate(blessingPrefab);
            Blessing blessing = blessingGO.GetComponent<Blessing>();
            
            if (blessing == null)
            {
                return;
            }

            blessing.transform.SetParent(BlessingsParent);
            blessing.transform.position = position;
            blessing.transform.rotation = rotation;
            
            blessing.Init(this, Blessings.Count);

            Blessings.Add(blessing);
        }

        public void RemoveBlessing(int index)
        {
            if (Blessings.Count > index)
            {
                for (int i = index; i < Blessings.Count; i++)
                {
                    Blessings[i].Index--;
                }

                Destroy(Blessings[index].gameObject);
                Blessings.RemoveAt(index);
            }
        }

        public void GameOver()
        {
            GameOverWindow.Open("You lost your whole herd...");
            Time.timeScale = 0.0f;
        }

        private void GameFinished()
        {
            GameOverWindow.Open("You saved " + Herd.GetCount(CharacterType.Goat) + " goats.");
            Time.timeScale = 0.0f;
        }
    }
}
