﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ludum
{
    public class Window : MonoBehaviour
    {
        public Text text;

        private float _Timer = 0;

        private void Update()
        {
            if (_Timer > 0)
            {
                //Debug.Log(_Timer);
                _Timer -= Time.deltaTime;
                if (_Timer <= 0)
                {
                    _Timer = 0;
                    Close();
                }
            }
        }

        public void Open(string message, float timer = 0)
        {
            _IsOpen = true;
            gameObject.SetActive(true);
            text.text = message;
            _Timer = timer;
        }

        public void Close()
        {
            _IsOpen = false;
            gameObject.SetActive(false);
        }

        private bool _IsOpen = false;
        public bool IsOpen
        {
            get
            {
                return _IsOpen;
            }
        }
    }
}