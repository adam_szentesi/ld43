﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Ludum
{
    public class Cursor : MonoBehaviour
    {
        public void Move(Vector3 position)
        {
            transform.position = position;
        }
    }
}
