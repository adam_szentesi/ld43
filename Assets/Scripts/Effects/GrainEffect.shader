﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/GrainEffect"
{
	Properties
	{
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}

	SubShader
	{
		Cull Off
		ZWrite Off
		ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float2 worldPos : TEXCOORD1;
			};

			v2f vert(appdata input)
			{
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.uv = input.uv;
				output.worldPos = input.vertex;
				return output;
			}

			sampler2D _MainTex;
			sampler2D _CameraDepthTexture;

			fixed4 frag(v2f input) : SV_Target
			{
				float2 offset = float2(0.5f, 0.5f);
				fixed4 col = tex2D(_MainTex, input.uv);

				//float4 pointCoord = float4(input.depth, 0, 0);
				//float depth = Linear01Depth(UNITY_SAMPLE_DEPTH(tex2D(_CameraDepthTexture, uv)));
				//fixed4 col = pointCoord;
				//col = 1 - col;

				float camDist = distance(input.worldPos, _WorldSpaceCameraPos);
				//col.r = camDist;



				return col;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}
