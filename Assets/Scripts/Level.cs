﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    public class Level : MonoBehaviour
    {
        public Collider LevelCollider;

        public KeyValuePair<bool, Vector3> GetPositionOnSurface(Vector3 rayOrigin)
        {
            KeyValuePair<bool, Vector3> result;

            Ray ray = new Ray(new Vector3(rayOrigin.x, 10, rayOrigin.z), Vector3.down);
            RaycastHit raycastHit;

            if (LevelCollider.Raycast(ray, out raycastHit, 20))
            {
                result = new KeyValuePair<bool, Vector3>(true, raycastHit.point);
            }
            else
            {
                result = new KeyValuePair<bool, Vector3>(false, new Vector3());
            }

            return result;
        }
    }
}