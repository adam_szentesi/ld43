﻿using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    public class Player : MonoBehaviour
    {
        public Level CurrentLevel;
        public float speed = 0.5f;

        void Update ()
        {
            HandleInput();
            Reposition();
	    }

        private void HandleInput()
        {
            if (Controller.GetAction(ControllerAction.MoveForward))
            {
                Move(Vector3.forward);
            }
            else if (Controller.GetAction(ControllerAction.MoveBackward))
            {
                Move(Vector3.back);
            }
        }

        private void Move(Vector3 moveDirection)
        {
            transform.localPosition += moveDirection * speed * Time.deltaTime;
        }

        private void Reposition()
        {
            KeyValuePair<bool, Vector3> newPositionData = CurrentLevel.GetPositionOnSurface(transform.position);

            if (newPositionData.Key)
            {
                transform.position = newPositionData.Value;
            }
        }

    }
}