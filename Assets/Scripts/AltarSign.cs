﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltarSign : MonoBehaviour
{
    public float amplitude;
    public float frequency;

    private Vector3 _OriginalPosition;

    private void Start()
    {
        _OriginalPosition = transform.localPosition;
    }

    void Update ()
    {
        transform.localPosition = _OriginalPosition + new Vector3(0, Mathf.Sin(Time.time * frequency) * amplitude, 0);
	}

    public void Select()
    {
        Color iconColor = GetComponent<SpriteRenderer>().color;
        iconColor.a = 1.0f;
        GetComponent<SpriteRenderer>().color = iconColor;
    }

    public void Unselect()
    {
        Color iconColor = GetComponent<SpriteRenderer>().color;
        iconColor.a = 0.5f;
        GetComponent<SpriteRenderer>().color = iconColor;
    }

    public void SetIcon(Sprite deityIcon)
    {
        GetComponent<SpriteRenderer>().sprite = deityIcon;
    }

}
