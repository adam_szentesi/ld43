﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    public class LevelTerrain : MonoBehaviour
    {
        public Collider TerrainCollider;
        public List<Altar> Altars = new List<Altar>();

        private void Start()
        {
            foreach (Altar altar in Altars)
            {
                KeyValuePair<bool, Vector3> newPositionData = GetPositionOnSurface(altar.transform.position);
                if (newPositionData.Key)
                {
                    altar.transform.position = newPositionData.Value;
                }
            }
        }

        public Vector3 GetPositionFromMouse(Vector3 screenPosition, ref bool success)
        {
            Vector3 result = Vector3.zero;
            Ray ray = Camera.main.ScreenPointToRay(screenPosition);
            RaycastHit raycastHit;

            if (TerrainCollider.Raycast(ray, out raycastHit, 1000))
            {
                result = raycastHit.point;
                success = true;
            }

            return result;
        }

        public KeyValuePair<bool, Vector3> GetPositionOnSurface(Vector3 XZPosition)
        {
            KeyValuePair<bool, Vector3> result;

            Ray ray = new Ray(new Vector3(XZPosition.x, 50, XZPosition.z), Vector3.down);
            RaycastHit raycastHit;

            if (TerrainCollider.Raycast(ray, out raycastHit, 1000))
            {
                result = new KeyValuePair<bool, Vector3>(true, raycastHit.point);
            }
            else
            {
                Debug.LogWarning("no contact");
                result = new KeyValuePair<bool, Vector3>(false, new Vector3());
            }

            return result;
        }

        public Altar FindAltar(Vector3 screenPosition, LevelTerrain terrain, ref bool success)
        {
            Altar result = null;

            foreach (Altar altar in Altars)
            {
                Ray ray = Camera.main.ScreenPointToRay(screenPosition);
                RaycastHit raycastHit;

                if (altar.Col.Raycast(ray, out raycastHit, 1000))
                {
                    result = raycastHit.collider.gameObject.GetComponent<Altar>();
                    success = true;
                    return result;
                }
            }

            success = false;
            return result;
        }

    }
}