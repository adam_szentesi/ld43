﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    public enum CharacterType
    {
        Undefined,
        Human,
        Goat,
        Crop,
    }

    public class Character : Being
    {
        

        //protected CharacterType _Type;
        public CharacterType Type;

    }
}