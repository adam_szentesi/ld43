﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    public class Being : MonoBehaviour
    {
        public SpriteRenderer HealthBar;
        public int Health = 100;
        private int _OriginalHealth = 1;

        public AudioSource DeathSound;
        public int AttackPower = 1;
        public float ReloadTime = 1.0f;
        private float _LastAttack = 0;
        protected Character _ActiveTarget = null;

        public float Speed = 0.5f;
        public float PersonalSpace = 0.5f;

        private bool _OnTarget = false;
        public bool OnTarget
        {
            get
            {
                return _OnTarget;
            }
        }

        private bool _JustArived = false;
        public bool JustArrived
        {
            get
            {
                return _JustArived;
            }
        }

        private void Start()
        {
            if (Health != 0)
            {
                _OriginalHealth = Health;
            }

            UpdateHealthBar(true);
        }

        public virtual void Update()
        {
            if (_LastAttack > 0)
            {
                _LastAttack -= Time.deltaTime;
            }
        }

        public void Init(LevelTerrain levelTerrain)
        {
            MoveTo(transform.position, levelTerrain);
        }

        public void TakeDamage(int damage)
        {
            if (IsAlive)
            {
                Health -= damage;
            }

            if (Health > 0)
            {
                UpdateHealthBar();
            }
            else
            {
                Die();
            }
        }

        public void Heal(int health)
        {
            if (IsAlive)
            {
                Health += health;
                if (Health > _OriginalHealth)
                {
                    Health = _OriginalHealth;
                }
                UpdateHealthBar();
            }
        }

        public void Attack()
        {
            if (_LastAttack > 0 || !IsAlive)
            {
                return;
            }

            if (_ActiveTarget != null)
            {
                _LastAttack = ReloadTime;
                _ActiveTarget.TakeDamage(AttackPower);

                if (!_ActiveTarget.IsAlive)
                {
                    _ActiveTarget = null;
                }
            }
        }

        protected void UpdateHealthBar(bool heathIsFull = false)
        {
            if (heathIsFull)
            {
                HealthBar.size = new Vector3(1.0f, 0.2f);
            }
            else
            {
                float percent = ((float)Health / _OriginalHealth);
                HealthBar.size = new Vector3(percent, 0.2f);
            }
        }

        protected virtual void Die()
        {
            DeathSound.Play();
            Health = 0;
            HealthBar.gameObject.SetActive(false);
            transform.rotation = Quaternion.Euler(0, 0, 90);
        }

        public bool IsAlive
        {
            get
            {
                return (Health > 0);
            }
        }

        public Vector3 GetNextPosition(Vector3 targetPosition)
        {
            Vector3 deltaVector = targetPosition - transform.position;
            Vector3 nextDeltaVector = (deltaVector).normalized * Speed * Time.deltaTime;
            Vector3 nextDistanceVector = nextDeltaVector + transform.position;

            if (deltaVector.sqrMagnitude > nextDeltaVector.sqrMagnitude)
            {
                _OnTarget = false;
                _JustArived = false;
                return nextDistanceVector;
            }

            if (_OnTarget)
            {
                _JustArived = false;
            }
            else
            {
                _JustArived = true;
            }

            _OnTarget = true;
            return targetPosition;
        }

        public void MoveTo(Vector3 targetPosition, LevelTerrain targetTerrain)
        {
            if (_LastAttack > 0 || !IsAlive)
            {
                return;
            }

            KeyValuePair<bool, Vector3> newPositionData = targetTerrain.GetPositionOnSurface(targetPosition);
            if (newPositionData.Key)
            {
                transform.position = newPositionData.Value;
            }
        }

        public void LookAt(Vector3 targetPosition)
        {
            Vector3 forward = targetPosition - transform.position;
            if (forward != Vector3.zero)
            {
                transform.rotation = Quaternion.LookRotation(forward, Vector3.up);
            }
        }

    }
}