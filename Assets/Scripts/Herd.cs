﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    public class Herd : MonoBehaviour
    {
        public Shepherd HerdShepherd;
        
        public List<Character> CharactersOnStart = new List<Character>();
        private Dictionary<CharacterType, List<Character>> _Characters = new Dictionary<CharacterType, List<Character>>();

        private Altar _TargetAltar;
        public Altar TargetAltar
        {
            get
            {
                return _TargetAltar;
            }
            set
            {
                if (_TargetAltar != null)
                {
                    Game.DeactivateAltar(_TargetAltar);
                    _TargetAltar.Unselect();
                }

                _TargetAltar = value;

                if (value != null)
                {
                    _TargetAltar.Select();
                }
            }
        }

        private Game Game;

        private void Start()
        {
            foreach (CharacterType characterType in (CharacterType[])Enum.GetValues(typeof(CharacterType)))
            {
                _Characters.Add(characterType, new List<Character>());
            }

            foreach (Character character in CharactersOnStart)
            {
                AddCharacter(character);
            }
        }

        private void AddCharacter(Character character)
        {
            if (character != null)
            {
                _Characters[character.Type].Add(character);
            }
        }

        public void Init(Game game)
        {
            Game = game;
        }

        private void Update()
        {
            //move shepherd
            Vector3 shepherdTargetPosition;
            if (_TargetAltar != null)
            {
                shepherdTargetPosition = _TargetAltar.transform.position;
            }
            else
            {
                shepherdTargetPosition = Game.GameCursor.transform.position;
            }

            Vector3 nextShepherdPosition = HerdShepherd.GetNextPosition(shepherdTargetPosition);
            HerdShepherd.MoveTo(nextShepherdPosition, Game.LevelTerrain);

            if (_TargetAltar != null && HerdShepherd.JustArrived)
            {
                Game.ActivateAltar(_TargetAltar);
            }

            //move herd
            foreach (List<Character> characters in _Characters.Values)
            {
                for (int i = characters.Count - 1; i >= 0 ; i--)
                {
                    if (!characters[i].IsAlive)
                    {
                        Kill(characters[i].Type, i);
                    }
                    else
                    {
                        Vector3 nextCharacterPosition = characters[i].GetNextPosition(HerdShepherd.transform.position);

                        if (!IsTooCloseToHerder(characters[i]) && !IsTooCloseToOtherCharacters(nextCharacterPosition, characters[i], i))
                        {

                            characters[i].MoveTo(nextCharacterPosition, Game.LevelTerrain);
                        }

                        characters[i].LookAt(HerdShepherd.transform.position);
                    }
                }
            }
        }

        private bool IsTooCloseToHerder(Character character)
        {
            float distanceSquared = (character.transform.position - HerdShepherd.transform.position).sqrMagnitude;
            if (distanceSquared < HerdShepherd.PersonalSpace * HerdShepherd.PersonalSpace)
            {
                return true;
            }
            return false;
        }

        private bool IsTooCloseToOtherCharacters(Vector3 nextCharacterPosition, Character character, int characterIndex)
        {
            float characterPersonalSpaceSquared = character.PersonalSpace * character.PersonalSpace;

            foreach (List<Character> characters in _Characters.Values)
            {
                for (int i = 0; i < characters.Count; i++)
                {
                    if(character != characters[i])
                    {
                        float spaceBetweenSquared = (nextCharacterPosition - characters[i].transform.position).sqrMagnitude;

                        float personalSpace = Mathf.Max(characterPersonalSpaceSquared, characters[i].PersonalSpace);
                        float personalSpaceSquared = personalSpace * personalSpace;

                        if (spaceBetweenSquared < personalSpaceSquared)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public string Sacrifice(CharacterType sacrificeType)
        {
            string sacrificeMessage = "You have nothing to offer to " + _TargetAltar.Deity.Name + ".";

            List<Character> characters = _Characters[sacrificeType];

            //nothing to offer
            if (characters.Count < 1)
            {
                return sacrificeMessage;
            }

            int index = characters.Count - 1;
            
            GameObject blessing = _TargetAltar.Sacrifice(characters[index], ref sacrificeMessage);
            if (blessing != null)
            {
                //summon blessing
                Game.SummonBlessing(blessing, characters[index].transform.position, characters[index].transform.rotation);

                //kill the sacrifice
                Kill(sacrificeType, index, true);
            }

            return sacrificeMessage;
        }

        private void Kill(CharacterType sacrificeType, int index, bool destroyObject = false)
        {
            if (destroyObject)
            {
                Destroy(_Characters[sacrificeType][index].gameObject);
            }

            _Characters[sacrificeType].RemoveAt(index);

            if (_Characters[CharacterType.Goat].Count == 0)
            {
                Game.GameOver();
            }
        }

        public Character FindClosest(Vector3 point, float maxDistance)
        {
            Character result = null;
            float minDistanceSquared = maxDistance * maxDistance;

            foreach (List<Character> characters in _Characters.Values)
            {
                foreach (Character character in characters)
                {
                    float distanceSquared = (point - character.transform.position).sqrMagnitude;
                    if (distanceSquared <= minDistanceSquared)
                    {
                        minDistanceSquared = distanceSquared;
                        result = character;
                    }
                }
            }

            return result;
        }

        public void Heal(int health)
        {
            Character characterToHeal = null;
            float lowestHealth = 9999999;

            foreach (List<Character> characters in _Characters.Values)
            {
                foreach (Character character in characters)
                {
                    if (character.Health < lowestHealth)
                    {
                        characterToHeal = character;
                        lowestHealth = character.Health;
                    }
                }
            }

            
            if (characterToHeal != null)
            {
                characterToHeal.Heal(health);
            }
        }

        public int GetCount(CharacterType characterType)
        {
            return _Characters[characterType].Count;
        }

    }
}