﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    public class Enemy : Being
    {
        public float LineOfSight = 10;

        public override void Update()
        {
            if (IsCloseEnoughToAttack())
            {
                Attack();
            }

            base.Update();
        }

        public void HuntTarget(Herd herd, LevelTerrain levelTerrain)
        {
            if (!IsAlive)
            {
                return;
            }

            if (_ActiveTarget != null)
            {
                Vector3 nextPosition = GetNextPosition(_ActiveTarget.transform.position);
                MoveTo(nextPosition, levelTerrain);
                LookAt(_ActiveTarget.transform.position);
            }
            else
            {
                Character closestTarget = herd.FindClosest(transform.position, LineOfSight);

                if (closestTarget != null && closestTarget.IsAlive)
                {
                    _ActiveTarget = closestTarget;
                }
                else
                {
                    _ActiveTarget = null;
                }
            }
        }

        private bool IsCloseEnoughToAttack()
        {
            if (_ActiveTarget == null)
            {
                return false;
            }

            float commonPersonalSpaceSquared = (_ActiveTarget.PersonalSpace + PersonalSpace);
            float spaceBetweenSquared = (transform.position - _ActiveTarget.transform.position).sqrMagnitude;
            if (spaceBetweenSquared < commonPersonalSpaceSquared)
            {
                return true;
            }

            return false;
        }

    }
}