﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    public class WindowManager : MonoBehaviour
    {
        public Canvas Canvas;
        public Window SacrificeWindow;

        public void OpenWindow(string message, float timer = 0)
        {
            SacrificeWindow.Open(message, timer);
        }

        public void CloseWindow()
        {
            SacrificeWindow.Close();
        }

        public bool IsOpen()
        {
            return SacrificeWindow.IsOpen;
        }

    }
}