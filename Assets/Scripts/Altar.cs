﻿using UnityEngine;

namespace Ludum
{
    public class Altar : MonoBehaviour
    {
        public Collider Col;
        public AltarSign SelectedSign;

        public Material BasicMaterial;
        public Material SelectedMaterial;

        public DeityData Deity;

        private bool _Active = false;
        public bool Active
        {
            get
            {
                return _Active;
            }
            set
            {
                if (!_Active && value)
                {
                    _Active = true;
                    GetComponent<MeshRenderer>().material = SelectedMaterial;
                }

                if (_Active && !value)
                {
                    _Active = false;
                    GetComponent<MeshRenderer>().material = BasicMaterial;
                }
            }
        }

        private void Start()
        {
            Unselect();
            SetIcon();
        }

        public void Select()
        {
            SelectedSign.Select();
        }

        public void Unselect()
        {
            SelectedSign.Unselect();
        }

        public GameObject Sacrifice(Character character, ref string message)
        {
            if (Deity.AcceptedOffering != character.Type)
            {
                message = Deity.Name + " does not accept your " + character.Type + ".";
                return null;
            }

            message = Deity.Name + " accepts your " + character.Type + " and sends a blessing.";

            return Deity.BlessingPrefab;
        }

        private void OnValidate()
        {
            SetIcon();
        }

        private void SetIcon()
        {
            if (Deity != null)
            {
                SelectedSign.SetIcon(Deity.DeityIcon);
            }
        }

    }
}