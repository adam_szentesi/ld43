﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum
{
    [CreateAssetMenu(fileName = "Deity", menuName = "Ludum/Deity", order = 1)]
    public class DeityData : ScriptableObject
    {
        public Deity Name;
        public Sprite DeityIcon;
        public CharacterType AcceptedOffering;
        public GameObject BlessingPrefab;
    }
}