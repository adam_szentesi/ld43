﻿Shader "Ludum/Cursor"
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
		}
		
		Lighting Off
		Fog {Mode Off}
		LOD 100

		Pass
		{
			Cull Off
			ZWrite Off
			Ztest Always

			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 position : SV_POSITION;
			};

			struct f2o
			{
				fixed4 color : SV_Target;
			};

			float4 _Color;

			v2f vert(appdata input)
			{
				v2f output;

				output.position = UnityObjectToClipPos(input.vertex);
				return output;
			}

			f2o frag(v2f input)
			{
				f2o output;

				output.color = _Color;
				return output;
			}

			ENDCG
		}
	}
}
